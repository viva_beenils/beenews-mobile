import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { PublicationSee } from "../../models/publication-see";
import { UserProvider } from "../user/user";
import { Params } from "../../utils/params";

@Injectable()
export class PublicationSeeProvider {
  private savedPublications: PublicationSee[] = [];

  constructor(public http: HttpClient, private userProvider: UserProvider) {}

  getSavedPublications() {
    let url = Params.getBaseUrl() + "/v1/publication_see/";
    let headers = new HttpHeaders({
      Authorization: "Bearer " + this.userProvider.getToken()
    });

    let params = new HttpParams();

    params = params.append("user_id", this.userProvider.getUser().id.toString());
    params =  params.append("status", "false");

    return this.http.get(url, { headers: headers, params: params });
  }

  public extractData(data) {
    let savedPublications = data["items"];
    let pubs = [];

    savedPublications.forEach(publication => {
      pubs.push(
        new PublicationSee(
          publication["id"],
          publication["publication_id"],
          publication["user_id"],
          publication["status"]["description"],
        )
      );
    });
    this.savedPublications = pubs;

    return pubs;
  }

  savePublication(publicationId: string){
    if(this.userProvider.isAuthenticated()){
      let url = Params.getBaseUrl() + "/v1/publication_see/create"; //verificar rota para criação
      let headers = new HttpHeaders({
        Authorization: "Bearer " + this.userProvider.getToken()
      });
      let form_data: FormData = new FormData();
      form_data.append("publication_id", publicationId);
      return this.http.post(url, form_data, { headers: headers });
    }
  }

}
