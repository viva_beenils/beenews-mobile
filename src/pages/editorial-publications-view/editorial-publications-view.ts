import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  Loading,
  LoadingController,
  ToastController
} from "ionic-angular";

import { Editorial } from "../../models/editorial";
import { Publication } from "../../models/publication";
import { PublicationProvider } from "../../providers/publication/publication";
import { UserProvider } from "../../providers/user/user";
import { Params } from "../../utils/params";
import { PublicationViewPage } from "../publication-view/publication-view";

/**
 * Generated class for the EditorialPublicationsViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-editorial-publications-view",
  templateUrl: "editorial-publications-view.html"
})
export class EditorialPublicationsViewPage {
  private loading: Loading;
  private editorial: Editorial;
  private editorialPublications = [];
  private page: number;
  private front_url = Params.getFrontUrl();
  private btn_visible: boolean = true;
  public editorialBgColor: string;

  constructor(
    public navCtrl: NavController,
    private userProvider: UserProvider,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private pubProvider: PublicationProvider
  ) {
    this.editorial = this.navParams.get("editorial");
    this.editorialColor(this.editorial.id);
  }
  ionViewWillLoad() {
    this.page = 1;
    if (this.editorial) {
      this.createLoading("Carregando notícias...");

      let search: Array<{ name: string; value: string }> = [];
      search.push({ name: "editorial", value: this.editorial.id.toString() });
      search.push({ name: "type", value: Publication.PublicationType.news });
      this.pubProvider
        .getPublications("5", null, this.page.toString(), null, null, search)
        .subscribe(pubs => {
          this.loading.dismiss();
          this.editorialPublications = this.pubProvider.extractData(pubs);
        },
        error => {
          this.loading.dismiss();
          console.log(error);
          this.presentToast("Erro no carregamento de notícia.", 3000, "bottom");
        });
    }
  }

  ionViewDidLoad() {}

  ionViewCanEnter() {
    return this.userProvider.isAuthenticated();
  }

  publicationView(id) {
    if (id != null) {
      this.pubProvider.get(id).subscribe(pub => {
        let publication = this.pubProvider.formatResponse(pub);
        this.navCtrl.push(PublicationViewPage, { publication: publication });
      });
    }
  }

  moreNews() {
    let search: Array<{ name: string; value: string }> = [];
    search.push({ name: "editorial", value: this.editorial.id.toString() });
    this.page++;
    this.pubProvider
      .getPublications("5", null, this.page.toString(), null, null, search)
      .subscribe(data => {
        let pubs = this.pubProvider.extractData(data);
        pubs.forEach(pub => {
          this.editorialPublications.push(pub);
        });
        if (pubs.length == 0) {
          this.btn_visible = false;
          this.page--;
        }
      });
  }

  editorialColor(editorialId: number) {
    switch (editorialId) {
      case Editorial.CULTURA_ID:
        this.editorialBgColor = "bg-cultura";
        break;
      case Editorial.ENTRETENIMENTO_ID:
        this.editorialBgColor = "bg-entretenimento";
        break;
      case Editorial.ESPORTES_ID:
        this.editorialBgColor = "bg-esportes";
        break;
      case Editorial.NOTICIAS_ID:
      default:
        this.editorialBgColor = "bg-noticias";
        break;
    }
  }

  createLoading(msg: string) {
    this.loading = this.loadingCtrl.create({
      content: msg,
      duration: 10000,
    });
    this.loading.present();
  }

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }
}
