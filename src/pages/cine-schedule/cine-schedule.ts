import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Loading, LoadingController, ToastController } from "ionic-angular";
import { Movie } from "../../models/movie";
import { CineinsiteProvider } from "../../providers/cineinsite/cineinsite";
import { CineSchedule } from "../../models/cine-schedule";
import { CineRoom } from "../../models/cine-room";

/**
 * Generated class for the CineSchedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-cine-schedule",
  templateUrl: "cine-schedule.html"
})
export class CineSchedulePage {
  movie: Movie;
  rooms: Array<{ room: CineRoom; schedules: Array<CineSchedule> }>;
  private loading: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public cineProvider: CineinsiteProvider,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
  ) {
    this.movie = this.navParams.get("movie");
  }

  ionViewDidLoad() {
    this.createLoading("Carregando sessões...");
    this.cineProvider
      .getMovieSchedule(this.movie.id)
      .subscribe((data: Array<any>) => {
        this.loading.dismiss();
        data.forEach(d => {
          console.log(d);
          this.rooms = this.cineProvider.extractScheduleData(d);
        });
      },
      error => {
        this.loading.dismiss();
        this.presentToast("Erro ao carregar sessões.", 3000, "bottom");
        console.log(error);
      });
  }

  createLoading(msg: string) {
    this.loading = this.loadingCtrl.create({
      content: msg,
      duration: 10000
    });
    this.loading.present();
  }

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }
}
