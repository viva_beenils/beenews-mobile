import { Component } from "@angular/core";
import {
  NavController,
  NavParams,
  ToastController,
  LoadingController,
  Loading
} from "ionic-angular";
import { Cine } from "../../models/cine";
import { CineinsiteProvider } from "../../providers/cineinsite/cineinsite";
import { Movie } from "../../models/movie";
import { UserProvider } from "../../providers/user/user";
import { CineinsiteViewPage } from "../cineinsite-view/cineinsite-view";
import { CineSchedulePage } from "../cine-schedule/cine-schedule";
import { CineListPage } from "../cine-list/cine-list";

/**
 * Generated class for the CineinsitePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-cineinsite",
  templateUrl: "cineinsite.html"
})
export class CineinsitePage {
  private loading: Loading;
  movies: Array<Movie> = [];
  constructor(
    public navCtrl: NavController,
    public cineProvider: CineinsiteProvider,
    public userProvider: UserProvider,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController
  ) {}

  ionViewDidLoad() {
    this.createLoading("Carregando filmes...");
    this.movies = [];
    this.cineProvider.getAll().subscribe(
      (data: Array<Movie>) => {
        this.loading.dismiss();
        this.movies = data;
      },
      error => {
        this.loading.dismiss();
        console.log(error);
        this.presentToast("Erro ao carregar títulos.", 3000, "bottom");
      }
    );
  }

  ionViewCanEnter() {
    return this.userProvider.isAuthenticated();
  }
  movieView(movie: Movie) {
    this.navCtrl.push(CineinsiteViewPage, { movie: movie });
  }

  movieSchedule(movie: Movie) {
    this.navCtrl.push(CineSchedulePage, { movie: movie });
  }

  goToCineList() {
    this.navCtrl.push(CineListPage);
  }

  createLoading(msg: string) {
    this.loading = this.loadingCtrl.create({
      content: msg,
      duration: 10000
    });
    this.loading.present();
  }

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }
}
