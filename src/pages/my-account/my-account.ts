import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  IonicPage,
  Loading,
  LoadingController,
  ModalController,
  NavController,
  NavParams,
  ToastController,
} from 'ionic-angular';

import { User } from '../../models/user';
import { UserProvider } from '../../providers/user/user';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: "page-my-account",
  templateUrl: "my-account.html"
})
export class MyAccountPage {
  myAccountForm: FormGroup
  user: User
  private loading: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public formBuilder: FormBuilder,
    public userProvider: UserProvider,
    public toastCtrl: ToastController,
    private modalCtrl: ModalController,
  ) {
    this.user = this.navParams.data.user;
    this.createForm();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad MyAccountPage");
  }

  createForm(){
    this.myAccountForm = this.formBuilder.group({
      name: [this.user.name + ' ' + this.user.last_name, [Validators.required, Validators.pattern("^[a-zA-Z -']+")]],
      phone: [this.user.phone_number],
      about: [this.user.about, [Validators.required]]
    });
  }

  editAccount(){
    // this.createLoading("Atualizando dados...");
    let name: string[] = this.myAccountForm.value["name"].split(' ');
    let phone: string = this.myAccountForm.value["phone"];

    this.user.name = name.slice(0, -1).join(' ');
    this.user.last_name = name[name.length - 1];
    this.user.phone_number = phone.replace(/[\(\)\-\ ]/g,"");
    this.user.about = this.myAccountForm.value["about"];

    // this.userProvider.update(this.user)
    // .subscribe(resp => {
    //   this.loading.dismiss();
    //   this.presentToast("Dados salvos com sucesso!", 3000, "bottom");
    // },
    // error => {
    //   this.loading.dismiss();
    //   this.presentToast("Erro ao salvar os dados.", 3000, "bottom");
    // });

    var modal = this.modalCtrl.create("ConfirmPasswordPage", {
      animation: "slide-in-up",
      viewType: "bottom-sheet",
      enableBackdropDismiss: true,
      user: this.user
    });

    modal.present();

    modal.onDidDismiss((data) => {
      if(data)
        this.navCtrl.setRoot(HomePage);
    });
  }

  createLoading(msg: string) {
    this.loading = this.loadingCtrl.create({
      content: msg,
      duration: 10000,
    });
    this.loading.present();
  }

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }
}
