import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, ToastController, LoadingController } from 'ionic-angular';
import { Editorial } from '../../models/editorial';
import { UserProvider } from '../../providers/user/user';
import { PublicationProvider } from '../../providers/publication/publication';
import { PublicationViewPage } from '../publication-view/publication-view';
import { Publication } from '../../models/publication';
import { PublicationSeeProvider } from '../../providers/publication-see/publication-see';

/**
 * Generated class for the SavedPublicationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-saved-publications',
  templateUrl: 'saved-publications.html',
})
export class SavedPublicationsPage {
  loading: Loading;
  private savedPublications = [];
  private publications = [];
  private page: number;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
    private userProvider: UserProvider,
    private pubProvider: PublicationProvider,
    private pubSeeProvider: PublicationSeeProvider) {
      this.getSavedPublications();
  }

  ionViewCanEnter() {
    return this.userProvider.isAuthenticated();
  }

  ionViewWillLoad(){
    this.page = 1;

    this.savedPublications.forEach(
      savedPub => {
        this.pubProvider.get(savedPub.publication_id)
        .subscribe(pub => {
          this.publications.push(pub);
        });
      }
    );
  }

  publicationView(id) {
    if (id != null) {
      this.pubProvider.get(id).subscribe(pub => {
        let publication = this.pubProvider.formatResponse(pub);
        this.navCtrl.push(PublicationViewPage, { publication: publication });
      });
    }
  }

  getSavedPublications(){
    this.pubSeeProvider.getSavedPublications()
    .subscribe(saved => {
      this.savedPublications = this.pubSeeProvider.extractData(saved);
    });
  }

  createLoading(msg: string) {
    this.loading = this.loadingCtrl.create({
      content: msg,
      duration: 10000,
    });
    this.loading.present();
  }

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }

}
