import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SavedPublicationsPage } from './saved-publications';

@NgModule({
  declarations: [
    SavedPublicationsPage,
  ],
  imports: [
    IonicPageModule.forChild(SavedPublicationsPage),
  ],
})
export class SavedPublicationsPageModule {}
