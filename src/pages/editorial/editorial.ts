import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Loading,
  LoadingController,
  ToastController
} from "ionic-angular";
import { EditorialProvider } from "../../providers/editorial/editorial";
import { Editorial } from "../../models/editorial";
import { EditorialPublicationsViewPage } from "../editorial-publications-view/editorial-publications-view";

/**
 * Generated class for the EditorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-editorial",
  templateUrl: "editorial.html"
})
export class EditorialPage {
  private loading: Loading;
  private editorials;
  private new_editorial = new Editorial(0, "");
  private listing = true;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private edtProvider: EditorialProvider,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController
  ) {}

  changeAction() {
    this.listing = !this.listing;
  }
  ionViewDidLoad() {
    this.refreshData();
  }

  refreshData() {
    this.createLoading("Carregando notícias..");
    this.edtProvider.refreshData().subscribe(
      edts => {
        this.loading.dismiss();
        this.editorials = this.edtProvider.extractData(edts);
      },
      error => {
        this.loading.dismiss();
        console.log(error);
        this.presentToast("Erro no carregamento de notícias.", 3000, "bottom");
      }
    );
  }

  addEditorial() {
    this.edtProvider.set(this.new_editorial);
    this.new_editorial = new Editorial(0, "");
    this.listing = true;
    this.refreshData();
  }

  openNews(editorial: Editorial) {
    this.navCtrl.push(EditorialPublicationsViewPage, { editorial: editorial });
  }

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }

  createLoading(msg: string) {
    this.loading = this.loadingCtrl.create({
      content: msg,
      duration: 10000
    });
    this.loading.present();
  }
}
