import { Component } from "@angular/core";
import { NavController, NavParams, ToastController, Loading, LoadingController } from "ionic-angular";
import { Cine } from "../../models/cine";
import { CineinsiteProvider } from "../../providers/cineinsite/cineinsite";

@Component({
  selector: "page-cine-list",
  templateUrl: "cine-list.html"
})
export class CineListPage {
  private loading: Loading;
  cines: Cine[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private cineProvider: CineinsiteProvider,
    private toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    this.createLoading("Obtendo salas...");
    this.cineProvider.getAllCines().subscribe(data => {
      this.loading.dismiss();
      this.cines = this.cineProvider.extractDataCine(data["items"]);
    },
    error => {
      this.loading.dismiss();
      this.presentToast("Erro ao obter salas de cinema.", 3000, "bottom");
      console.log(error);
    }
    );
  }

  ionViewDidLoad() {}

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }

  createLoading(msg: string) {
    this.loading = this.loadingCtrl.create({
      content: msg,
      duration: 10000
    });
    this.loading.present();
  }
}
