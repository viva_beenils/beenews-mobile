import { Component, Renderer2 } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Push } from "@ionic-native/push";
import { Events, NavController, ToastController, Loading, LoadingController } from "ionic-angular";

import { UserProvider } from "../../providers/user/user";
import { HomePage } from "../home/home";
import { RecoverPage } from "../recover/recover";
import { SignupPage } from "../signup/signup";

/**
 * Generated class for the SigninPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-signin",
  templateUrl: "signin.html"
})
export class SigninPage {
  protected loginForm: FormGroup;
  private loading: Loading;

  constructor(
    public navCtrl: NavController,
    private userProvider: UserProvider,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private renderer: Renderer2,
    public loadingCtrl: LoadingController,
    private events: Events,
    private push: Push
  ) {
    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.email, Validators.required]],
      password: ["", Validators.required]
    });

    this.navCtrl.setRoot(HomePage);
  }

  login() {
    this.createLoading();
    let credentials = this.loginForm.value;
    this.userProvider
      .login(credentials["email"], credentials["password"])
      .subscribe(
        data => {
          this.loading.dismiss();
          this.userProvider.extractData(data);
          if (this.userProvider.isAuthenticated()) {
            this.presentToast(
              "Bem vindo, " + this.userProvider.getUser().name,
              2000,
              "bottom"
            );

            this.events.publish("user:logged", this.userProvider.getUser());
            this.navCtrl.setRoot(HomePage);
          }
        },
        error => {
          this.loading.dismiss();
          this.presentToast(
            "Erro ao acessar a conta: " + JSON.parse(error.error.message).email,
            //JSON.stringify(error),
            2000,
            "bottom"
          );

          this.userProvider.erroLogin();
        }
      );
  }

  ionViewDidLoad() {}

  ionViewWillLoad() {}

  goToRecover() {
    this.navCtrl.push(RecoverPage);
  }

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }

  goToSignUp() {
    this.navCtrl.push(SignupPage);
  }

  createLoading(){
    this.loading = this.loadingCtrl.create({
      content: "Entrando na conta...",
      duration: 10000,
    });
    this.loading.present();
  }


}
