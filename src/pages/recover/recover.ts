import { Component } from "@angular/core";
import { NavController, NavParams, ToastController, Loading, LoadingController } from "ionic-angular";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserProvider } from "../../providers/user/user";
import { SigninPage } from '../signin/signin';

/**
 * Generated class for the RecoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-recover",
  templateUrl: "recover.html"
})
export class RecoverPage {
  recoverForm: FormGroup;
  private loading: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    private userProvider: UserProvider,
    private toastCtrl: ToastController
  ) {
    this.recoverForm = this.formBuilder.group({
      email: ["", [Validators.email, Validators.required]]
    });
  }

  ionViewDidLoad() {}

  recover() {
    this.createLoading("Carregando...");
    let data = this.recoverForm.value;
    this.userProvider.recoverPassword(data["email"]).subscribe(
      data => {
        this.loading.dismiss();
        this.presentToast(
          "Siga as instruções em seu email para recuperar a senha",
          3000,
          "BOTTOM"
        );
        this.navCtrl.setRoot(SigninPage);
      },
      error => {
        this.loading.dismiss();
        this.presentToast(
          "Erro na recuperação de senha:" + error.message,
          3000,
          "bottom"
        )
      }
    );
  }

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }

  createLoading(msg: string) {
    this.loading = this.loadingCtrl.create({
      content: msg,
      duration: 10000,
    });
    this.loading.present();
  }
}
