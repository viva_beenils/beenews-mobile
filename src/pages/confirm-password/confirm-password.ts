import { UserProvider } from './../../providers/user/user';
import { FormGroup } from '@angular/forms';
import { User } from './../../models/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, ToastController, ViewController } from 'ionic-angular';
import { HomePage } from '../home/home';

/**
 * Generated class for the ConfirmPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirm-password',
  templateUrl: 'confirm-password.html',
})
export class ConfirmPasswordPage {
  user: User;
  password: any;
  loading: Loading;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private userProvider: UserProvider,
    public viewCtrl: ViewController,) {
    this.user = this.navParams.data.user;
  }

  closeModal(){
    console.log("closeModal");
    this.viewCtrl.dismiss();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmPasswordPage');
  }

  confirmPassword(){
    this.createLoading("Atualizando dados...");

    this.userProvider.update(this.user, this.password)
    .subscribe(resp => {
      this.loading.dismiss();
      this.presentToast("Dados salvos com sucesso!", 3000, "bottom");
      this.viewCtrl.dismiss("Saved");
    },
    error => {
      this.loading.dismiss();
      this.presentToast("Erro ao salvar os dados.", 3000, "bottom");
    });
  }

  createLoading(msg: string) {
    this.loading = this.loadingCtrl.create({
      content: msg,
      duration: 10000,
    });
    this.loading.present();
  }

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }

}
