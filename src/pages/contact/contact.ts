import { HomePage } from './../home/home';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { UserProvider } from "./../../providers/user/user";
import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams, Loading, ToastController, LoadingController } from "ionic-angular";

@IonicPage()
@Component({
  selector: "page-contact",
  templateUrl: "contact.html"
})
export class ContactPage {
  contactForm: FormGroup;
  subjectOptions: Array<string>;
  loading: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private userProvider: UserProvider,
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private loadingCtrl: LoadingController,
  ) {
    this.createForm();
    this.subjectOptions = ["Críticas", "Dúvidas", "Elogios", "Reclamações", "Sugestões"];
  }

  sendMessage() {
    this.createLoading("Enviando mensagem...");

    let subject = this.contactForm.value["subject"];
    let message = this.contactForm.value["message"];

    this.userProvider.contact(subject, message)
    .subscribe(
      data => {
        this.loading.dismiss();
        this.presentToast("Mensagem enviada com sucesso!", 3000, "bottom");
        this.navCtrl.pop();
      },
      error => {
        this.loading.dismiss();
        this.presentToast("Erro no envio da mensagem.", 3000, "bottom");
      }
    );
  }

  createForm() {
    this.contactForm = this.formBuilder.group({
      subject: ["", Validators.required],
      message: ["", Validators.required]
    });
  }

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }

  createLoading(msg: string) {
    this.loading = this.loadingCtrl.create({
      content: msg,
      duration: 10000
    });
    this.loading.present();
  }

}
