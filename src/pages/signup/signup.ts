import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import {
  Events,
  NavController,
  NavParams,
  ToastController,
  Loading
} from "ionic-angular";

import { User } from "../../models/user";
import { UserProvider } from "../../providers/user/user";
import { SigninPage } from "../signin/signin";
import { LoadingController } from "ionic-angular";

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: "page-signup",
  templateUrl: "signup.html"
})
export class SignupPage {
  private loginForm: FormGroup;
  private loading: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private userProvider: UserProvider,
    private formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    private events: Events
  ) {
    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.email, Validators.required]],
      name: ["", [Validators.required, Validators.pattern("^[a-zA-Z -']+")]],
      password: ["", [Validators.required, Validators.minLength(5)]],
      password_repeat: ["", [Validators.required]]
    });
  }

  ionViewDidLoad() {}

  signUp() {
    this.createLoading("Criando nova conta...");
    let formData = this.loginForm.value;
    let name = formData.name.split(" ");

    let u: User = new User(0, name.slice(0,-1).join(' '), name[name.length - 1], 0, 0);
    u.email = formData.email;
    let credentials: { password: string; password_repeat: string } = {
      password: formData.password,
      password_repeat: formData.password_repeat
    };

    this.userProvider.signUp(u, credentials).subscribe(
      resp => {
        this.loading.dismiss();
        this.presentToast(
          "Vá até a sua caixa de entrada (" +
            u.email +
            ") para ativar sua conta",
          4000,
          "bottom"
        );
        this.navCtrl.setRoot(SigninPage);
      },
      error => {
        this.loading.dismiss();
        this.presentToast(
          "Erro na criação da conta.",
          4000,
          "bottom");
      }
    );
  }

  presentToast(message: string, duration: number, position: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });

    toast.present();
  }

  createLoading(msg: string) {
    this.loading = this.loadingCtrl.create({
      content: msg,
      duration: 10000,
    });
    this.loading.present();
  }
}
