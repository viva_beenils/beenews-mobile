export class PublicationSee {
  private _id: number;
  private _publication_id: string;
  private _user_id: string;
  private _status: string;

  constructor(id: number, publication_id: string, user_id: string, status: string) {
    this._id = id;
    this._publication_id = publication_id;
    this._user_id = user_id;
    this._status = status;
  }

  get id(): number {
    return this._id;
  }

  get publication_id(): string {
    return this._publication_id ;
  }

  get user_id(): string {
    return this._user_id;
  }

  get status(): string {
    return this._status;
  }
}
