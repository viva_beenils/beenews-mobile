import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AdMobFree } from '@ionic-native/admob-free';
import { Firebase } from '@ionic-native/firebase';
import { Push } from '@ionic-native/push';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MomentModule } from 'angular2-moment';
import { BrMaskerIonic3, BrMaskerModule } from 'brmasker-ionic-3';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { ComponentsModule } from '../components/components.module';
import { ApproveCommentsPage } from '../pages/approve-comments/approve-comments';
import { ApproveNewsPage } from '../pages/approve-news/approve-news';
import { CineListPage } from '../pages/cine-list/cine-list';
import { CineSchedulePage } from '../pages/cine-schedule/cine-schedule';
import { CineinsiteViewPage } from '../pages/cineinsite-view/cineinsite-view';
import { CineinsitePage } from '../pages/cineinsite/cineinsite';
import { EditorialPublicationsViewPage } from '../pages/editorial-publications-view/editorial-publications-view';
import { EditorialPage } from '../pages/editorial/editorial';
import { HomePage } from '../pages/home/home';
import { MyAccountPageModule } from '../pages/my-account/my-account.module';
import { PrivacyPolicyPageModule } from '../pages/privacy-policy/privacy-policy.module';
import { ProfilePage } from '../pages/profile/profile';
import { PublicationListPage } from '../pages/publication-list/publication-list';
import { PublicationPreviewPage } from '../pages/publication-preview/publication-preview';
import { PublicationViewPage } from '../pages/publication-view/publication-view';
import { RecoverPage } from '../pages/recover/recover';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import { SubmitNewsPage } from '../pages/submit-news/submit-news';
import { TermsOfServicePageModule } from '../pages/terms-of-service/terms-of-service.module';
import { CineinsiteProvider } from '../providers/cineinsite/cineinsite';
import { CommentaryProvider } from '../providers/commentary/commentary';
import { EditorialProvider } from '../providers/editorial/editorial';
import { FcmProvider } from '../providers/fcm/fcm';
import { PublicationReactionProvider } from '../providers/publication-reaction/publication-reaction';
import { PublicationProvider } from '../providers/publication/publication';
import { UserProvider } from '../providers/user/user';
import { ConfirmPasswordPageModule } from './../pages/confirm-password/confirm-password.module';
import { ContactPageModule } from './../pages/contact/contact.module';
import { MyApp } from './app.component';
import { PublicationSeeProvider } from '../providers/publication-see/publication-see';


//const config: SocketIoConfig = {url:Params.getWebSocketUrl(), options:{}};
const firebase = {
  apiKey: "",
  authDomain: "",
  databaseURL: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: ""
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    PublicationViewPage,
    EditorialPage,
    PublicationListPage,
    PublicationViewPage,
    EditorialPublicationsViewPage,
    SignupPage,
    SigninPage,
    ApproveNewsPage,
    ProfilePage,
    SubmitNewsPage,
    ApproveCommentsPage,
    PublicationPreviewPage,
    CineinsitePage,
    CineinsiteViewPage,
    CineSchedulePage,
    CineListPage,
    RecoverPage
  ],
  imports: [
    BrowserModule,
    //SocketIoModule.forRoot(config),
    HttpClientModule,
    ComponentsModule,
    MomentModule,
    BrMaskerModule,
    IonicModule.forRoot(MyApp),
    PrivacyPolicyPageModule,
    TermsOfServicePageModule,
    MyAccountPageModule,
    ConfirmPasswordPageModule,
    ContactPageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    PublicationViewPage,
    EditorialPage,
    PublicationViewPage,
    PublicationListPage,
    EditorialPublicationsViewPage,
    SigninPage,
    SignupPage,
    ApproveNewsPage,
    ProfilePage,
    SubmitNewsPage,
    ApproveCommentsPage,
    PublicationPreviewPage,
    CineinsitePage,
    CineSchedulePage,
    CineListPage,
    CineinsiteViewPage,
    RecoverPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    PublicationProvider,
    EditorialProvider,
    UserProvider,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    PublicationReactionProvider,
    CommentaryProvider,
    SocialSharing,
    Push,
    AdMobFree,
    Firebase,
    FcmProvider,
    CineinsiteProvider,
    BrMaskerIonic3,
    PublicationSeeProvider,
  ]
})
export class AppModule {}
